pre_processing: |-
  {%
    set output = {
        'NGFW': [], 'Threat_Prevention': [], 'Wildfire': [],
        'DNS_Security': [], 'URL_Filtering': [],
        'stats': {'total':0, 'pass':0, 'fail':0}
      }
  %}
  {% for check in data.keys() %}
    {% set c = data[check] %}
    {% if c.results %}
      {% set _= c.__setitem__('icon', 'checkmark') %}
      {% set _= output.stats.__setitem__('pass', output.stats.pass + 1) %}
    {% else %}
      {% set _= c.__setitem__('icon', 'alert') %}
      {% set _= output.stats.__setitem__('fail', output.stats.fail + 1) %}
    {% endif %}

    {# Map meta params onto root element required for report rendering #}
    {% set _= c.__setitem__('META_product', c.meta.product) %}

    {% set product = c.meta.product %}
    {% set _= output[product].append(c) %}
    {% set _= output.stats.__setitem__('total', output.stats.total + 1) %}
  {% endfor %}
  {{ output | tojson }}