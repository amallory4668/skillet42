"""
Load all unit 42 files and compile a list of COA's and validate mappings are found for all of them
"""

from skillet42.tools import parse_atom_file, load_settings, get_unit42_files, load_mappings, fw_products


def check_coa_mappings(settings, mappings):

    # Generate a list of all COA's in all files
    all_coa = []
    coaDict = {}

    for u42File in get_unit42_files(settings):
        atom = parse_atom_file(u42File)
        coa_ids = [x['id'] for x in atom['coa']]
        for i in coa_ids:
            iDict = [x for x in atom['coa'] if x['id'] == i][0]
            if any([x in fw_products for x in iDict['related_products']]):
                coaDict[i] = iDict
                all_coa.append(iDict['id'])
        if not len(coa_ids) > 0:
            raise ValueError(f"Did not find any COA Id's inside {u42File}")
    
    # for coa in coaDict:
    #     print(coa)
    #     print(coaDict[coa]['name'])
    #     print(coaDict[coa].get('description', ''))
    #     print('\n')

    all_coa = list(set(all_coa))

    # Validate parity between COA mapings and Unit 42

    no_mapping = [x for x in all_coa if not x in mappings]
    if len(no_mapping) > 0:
        raise Exception(f"Unit 42 COA's with no mapping - {', '.join(no_mapping)}")

    unused_mapping = [x for x in mappings if not x in all_coa]
    if len(unused_mapping) > 0:
        print(f"COA mappings not found in unit 42 - {', '.join(unused_mapping)}")

    print(f"All {len(all_coa)} COA mappings accounted for")

if __name__ == '__main__':

    settings = load_settings()
    mappings = load_mappings()
    check_coa_mappings(settings, mappings)