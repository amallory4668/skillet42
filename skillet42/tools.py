import os
import json
import yaml
from jinja2 import Template

fw_products = {'NGFW', 'Wildfire', 'Threat Prevention', 'URL Filtering', 'DNS Security'}

def process_text_block(text):
    """
    Modify a multi line parameter to fit in a YAML text block
    """
    return '   ' + '\n   '.join(text.split('\n')).strip()

def process_single_line(line):
    """
    Modify a single line parameter to be a valid YAML input
    """
    return line.replace('"', "")

def map_coa(snippet, mappings):
    """
    Map values from loaded mapping object onto snippet object
    """
    mapping = mappings.get(snippet['id'])
    if not isinstance(mapping, dict):
        return
    if mapping.get('not_implemented') == True:
        snippet['not_implemented'] = True
    else:
        for key in mapping:
            snippet[key] = mapping[key]

def parse_atom_file(file_name):
    """
    Load a Unit 42 file and reformat it to be ideal for skillet generation
    """
    
    # Load JSON document
    with open(file_name, 'r') as f:
        atom = json.load(f)

    # Get list of "course-of-action" objects
    coa = [ x for x in atom['objects'] if x['type'] == 'course-of-action' ]

    # Create a mapping list of dicts for objects based on source_ref
    rel_objs = [ x for x in atom['objects'] if x['type'] == 'relationship' ]
    rel_sources = list(set([ x['source_ref'] for x in rel_objs ]))
    rel = { x: [] for x in rel_sources }
    for r in rel_objs:
        rel[r['source_ref']].append(r)

    # Create a dict of all objects by ID for mapping
    objs_by_id = { x['id']: x for x in atom['objects'] }

    # Map relationship directly on top of "course-of-action" objects
    for c in coa:
        for r in rel[c['id']]:
            ref_name = r['target_ref']
            r_target_type = ref_name.split('--')[0]

            # Append referenced object to list of references by type
            if not r_target_type in c:
                c[r_target_type] = []
            c[r_target_type].append(objs_by_id[ref_name])

            # Append related products if present in reference object
            if not 'related_products' in c:
                c['related_products'] = []
            if isinstance(r.get("x_panw_coa_u42_panw_product", None), list):
                for product in r['x_panw_coa_u42_panw_product']:
                    if not product in c['related_products']:
                        c['related_products'].append(product)

    report = [ x for x in atom['objects'] if x['type'] == 'report' and 'description' in x ]
    if len(report) > 1:
        raise ValueError('More than 1 report object with description, needs to be reimplemented')
    return {
        "coa": coa,
        "report": report[0]
    }

def create_ngfw_skillet(atom, out_file, mappings):

    # Filter down to COA that can be implemented on NGFW platform
    fw_snippets = [ x for x in atom['coa'] if bool(fw_products & set(x['related_products'])) ]

    # Create snippets objects for rendering
    snippets = [] 
    for fws in fw_snippets:

        desc = fws.get('description')
        if desc:
            for r in ['\n', '\t']:
                desc = desc.replace(r, ' ')

        # Extract and include mitre phases
        phases = []
        for ap in fws['attack-pattern']:
            for kcp in ap['kill_chain_phases']:
                if kcp['kill_chain_name'] == 'mitre-attack':
                    if not kcp['phase_name'] in phases:
                        phases.append(kcp['phase_name'])
        phases = ', '.join(phases) if len(phases) else ''

        if len(fws['related_products']) > 1:
            raise ValueError(f"COA has more than 1 related_product, handling not implemented - {fws}")
        if len(fws['related_products']) < 1:
            raise ValueError(f"COA has no related_product, handling not implemented - {fws}")

        snippet =  {
                'id': fws['id'],
                'label': fws['name'],
                'fail_message': desc,
                'mitre_phases': phases,
                'related_product': fws['related_products'][0].replace(' ', '_')
            }
        map_coa(snippet, mappings)
        snippets.append(snippet)

    # Render skillet skeleton
    context = {
        'name': process_single_line(atom['report']['name']),
        'label': atom['report']['name'],
        'description': process_text_block(atom['report']['description']),
        'snippets': snippets
    }
    with open('validation_skeleton.j2', 'r') as f:
        template = Template(f.read())
    
    with open(out_file, 'w', encoding='utf-8') as f:
        f.write(template.render(context))

def get_unit42_files(settings):
    """
    Generator for unit42 files
    """
    ls = os.listdir(settings['unit42_directory'])
    for f in ls:
        if f.endswith('.json') and not f in settings.get('ignored_files', []):
            yield settings['unit42_directory'] + f


def load_settings(settings_file="settings.yaml"):
    """
    Load settings so calling scripts all have identical shared variables
    """
    with open(settings_file, 'r') as f:
        return yaml.safe_load(f)

def load_mappings(coa_mappings="coa_mappings.yaml"):
    """
    Load COA to snippet mappings
    """
    with open(coa_mappings, 'r') as f:
        return yaml.safe_load(f)