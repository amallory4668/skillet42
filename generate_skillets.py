"""
Generage Unit-42 skillets
"""

import shutil
import os
from jinja2 import Template

from skillet42.tools import parse_atom_file, create_ngfw_skillet, load_settings, get_unit42_files, load_mappings, fw_products

def generate_skillets(settings, mappings):

    # With the target repository as a submodule, remove old skillets
    output_directory = settings['output_directory'] + '/'
    if os.path.exists(output_directory):
        shutil.rmtree(output_directory)
    os.mkdir(output_directory)

    # Generate a new skillet for each ATOM

    for u42File in get_unit42_files(settings):
        title = ''.join(u42File.split('/')[-1].split('.')[:-1])
        atom = parse_atom_file(u42File)
        coa_ids = [x['id'] for x in atom['coa']]

        skilletDir = output_directory + title + os.path.sep
        os.mkdir(skilletDir)
        output_path = skilletDir + title + '.skillet.yaml'
        create_ngfw_skillet(atom, output_path, mappings)

        # Create local panforge dir
        reportDir = skilletDir + os.path.sep + 'report'
        os.mkdir(reportDir)

        # Render report.yml file
        products = []
        for coa in atom['coa']:
            products += coa.get('related_products', [])
        products = list(set(products))
        sections = {x: x.replace(' ', '_') for x in products if x in fw_products}
        with open('report.j2', 'r') as f:
            report_template = Template(f.read())
        rendered_template = report_template.render(subtitle=title, sections=sections)
        with open(reportDir + os.path.sep + 'report.yml', 'w') as f:
            f.write(rendered_template)

            # Add in pre_processing block. Stored seperately as it has jinja content
            f.write('\n\n')
            with open('pre_processing.txt', 'r') as ff:
                f.write(ff.read())
        
        # Render local panforge primitive with ATOM description
        with open('atom_description.j2', 'r') as f:
            atom_description_template = Template(f.read())
        atom_description_rendered = atom_description_template.render(description=atom['report']['description'])
        with open(reportDir + os.path.sep + 'atom_description.j2', 'w') as f:
            f.write(atom_description_rendered)
        
        # Copy unit42 logo into skillet report dir
        shutil.copy('unit42.png', reportDir)

if __name__ == '__main__':

    settings = load_settings()
    mappings = load_mappings()
    generate_skillets(settings, mappings)