"""
Validate created skillets actually load into skilletlib
"""

import os

from skilletlib import SkilletLoader
from skillet42.tools import load_settings

def validate_skillets(settings):

    output_directory = settings['output_directory'] + '/'
    count = len(os.listdir(output_directory))
    sl = SkilletLoader()
    skillets = sl.load_all_skillets_from_dir('./')
    loaded = len(skillets)

    if len(sl.skillet_errors):
        for err in sl.skillet_errors:
            print('Error:')
            for key, value in err.items():
                print(f'  {key}: {value}')
        raise Exception('SkilletLoader encountered errors, check logs')

    if loaded != count:
        raise Exception(f'Expected to load {count} skillets but got {loaded}')

    print(f'Validated and loaded {count} skillets')


if __name__ == '__main__':

    settings = load_settings()
    validate_skillets(settings)