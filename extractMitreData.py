import os
import json
from jinja2 import Template

from skillet42.tools import parse_atom_file

def run():

    mappings = {}
    fw_products = ['NGFW', 'Wildfire', 'Threat Prevention', 'URL Filtering', 'DNS Security']

    json_dir = 'playbook_viewer/playbook_json/'
    files = [x for x in os.listdir(json_dir) if x.endswith('.json')]
    for f in files:
        try:
            j = parse_atom_file(json_dir + f)
            coa = j['coa']
            for c in coa:
                for ap in c['attack-pattern']:
                    if ap['name'].startswith('T'):
                        name = ap['name']
                        for phase in ap['kill_chain_phases']:
                            pName = phase['phase_name']
                            if not pName in mappings:
                                mappings[pName] = {
                                    'T': [],
                                    'products':{}
                                    }
                            if not name in mappings[pName]['T']:
                                mappings[pName]['T'].append(name)
                            for p in c['related_products']:
                                if not p in fw_products:
                                    continue
                                if p not in mappings[pName]['products']:
                                    mappings[pName]['products'][p] = []
                                if not c['name'] in mappings[pName]['products'][p]:
                                    mappings[pName]['products'][p].append(c['name'])
                                
        except UnicodeDecodeError as e:
            print(f"{f} had a decoding error - {e}")

    # Remove phases that do not have any products
    # TODO: This should account for MITRE phases, just update this static list
    validPhases = ["initial-access", "execution", "persistence", "privilege-escalation", "defense-evasion", "credential-access", "discovery", "lateral-movement", "command-and-control", "exfiltration", "impact"]
    toRemove = []
    for p in mappings:
        if not len(mappings[p]['products'].keys()):
            toRemove.append(p)
        elif not p in validPhases:
            toRemove.append(p)
    for p in toRemove:
        mappings.pop(p)
    

    with open('mappings.json', 'w') as f:
        f.write(json.dumps(mappings, indent=4))
    
    # HTML output
    html = Template("""
<!DOCTYPE html>
<head>
 <title>COA by Phases</title>
 <style>
 table{
     border-collapse: collapse;
 }
 td{
     border:1px solid #000;
     padding: 10px;
 }
 .phase{
     background-color: #fa582d;
     color: #fff;
     text-align: center;
     font-size:40px;
     text-transform: uppercase;
 }
 </style>
</head>
<body>
</body>
 <table>
  <tbody>
   <tr><td>Product / Service</td><td>Course of Action</td></tr>
   {% for phase in ["initial-access", "execution", "persistence", "privilege-escalation", "defense-evasion", "credential-access", "discovery", "lateral-movement", "command-and-control", "exfiltration", "impact"] %}
    <tr><td colspan="2" class="phase">{{ phase }}</td></tr>
    <tr><td colspan="2" style="padding-left: 100px;">{% for t in mappings[phase]["T"] %}{{ t }}<br />{% endfor %}</td></tr>
    {% for p in mappings[phase]["products"] %}
     {% for c in mappings[phase]["products"][p] %}
      <tr>{% if loop.first %}<td rowspan="{{ mappings[phase]["products"][p] | length }}">{{p}}</td>{% endif %}<td>{{c}}</td></tr>
     {% endfor %}
    {% endfor %}
   {% endfor %}
  </tbody>
 </table>
</html>
    """).render(mappings=mappings)
    with open('mappings.html', 'w') as f:
        f.write(html)

if __name__ == '__main__':
    run()